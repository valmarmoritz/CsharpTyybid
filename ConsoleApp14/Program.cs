﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp14
{
    class Program
    {
        static void Main(string[] args)
        {
            int skoor = 0;
            skoor = 12;
            Console.WriteLine("skoor on "+skoor);

            int a = 5;
            int b = 2;

            b = a;
            a = -3;
            Console.WriteLine("a = "+a+", b = "+b);

            int i1, i2, i3;
            i1 = i2 = i3 = 11;
            int i4 = 14, i5 = 15, i6 = 16;
            Console.WriteLine("i1, i2, i3, i4, i5, i6 väärtused on: "+i1+" "+i2+" "+i3 + " " + i4 + " " + i5 + " " + i6);

            int varIntMin = int.MinValue; int varIntMax = int.MaxValue; Console.WriteLine("    int: min "+varIntMin+" max "+varIntMax);
            uint varUintMin = uint.MinValue; uint varUintMax = uint.MaxValue; Console.WriteLine("   uint: min "+varUintMin+" max "+varUintMax);
            short varShortMin = short.MinValue; short varShortMax = short.MaxValue; Console.WriteLine("  short: min "+ varShortMin + " max "+ varShortMax);
            ushort varUshortMin = ushort.MinValue; ushort varUshortMax = ushort.MaxValue; Console.WriteLine(" ushort: min "+ varUshortMin + " max "+ varUshortMax);
            long varLongMin = long.MinValue; long varLongMax = long.MaxValue; Console.WriteLine("   long: min "+ varLongMin + " max "+ varLongMax);
            ulong varUlongMin = ulong.MinValue; ulong varUlongMax = ulong.MaxValue; Console.WriteLine("  ulong: min "+ varUlongMin + " max "+ varUlongMax);
            sbyte varSbyteMin = sbyte.MinValue; sbyte varSbyteMax = sbyte.MaxValue; Console.WriteLine("  sbyte: min " + varSbyteMin + " max "+ varSbyteMax);
            byte varByteMin = byte.MinValue; byte varByteMax = byte.MaxValue; Console.WriteLine("   byte: min " + varByteMin + " max "+ varByteMax);
            char varCharMin = char.MinValue; char varCharMax = char.MaxValue; Console.WriteLine("   char: min " + varCharMin + " max "+ varCharMax);
            float varFloatMin = float.MinValue; float varFloatMax = float.MaxValue; Console.WriteLine("  float: min " + varFloatMin + " max " + varFloatMax);
            double varDoubleMin = double.MinValue; double varDoubleMax = double.MaxValue; Console.WriteLine(" double: min " + varDoubleMin + " max " + varDoubleMax);
            decimal varDecimalMin = decimal.MinValue; decimal varDecimalMax = decimal.MaxValue; Console.WriteLine("decimal: min " + varDecimalMin + " max " + varDecimalMax);

            char favoriteLetter = 'v'; Console.WriteLine("My favorite letter: "+favoriteLetter);
            favoriteLetter = '&'; Console.WriteLine("My favorite letter: "+favoriteLetter);

            string message = "This is a message."; Console.WriteLine(message);
        }
    }
}
